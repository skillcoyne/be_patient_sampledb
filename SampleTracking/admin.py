from django.contrib import admin

# Register your models here.

from .models import Patient
from .models import AlternatePatientId
from .models import Endoscopy
from .models import EndoscopySample
from .models import HistoPathSlides
from .models import SequenceMetadata


admin.site.register(Patient)
admin.site.register(AlternatePatientId)
admin.site.register(Endoscopy)
admin.site.register(EndoscopySample)
admin.site.register(HistoPathSlides)
admin.site.register(SequenceMetadata)
