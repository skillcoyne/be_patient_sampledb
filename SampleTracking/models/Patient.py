from django.db import models
from SampleTracking.fields import DateText
from django.core.validators import MaxValueValidator, MinValueValidator

class Patient(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    uuid = models.TextField(db_column='UUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    gender = models.CharField(db_column='Gender', max_length=6, choices=(('M', 'male'), ('F', 'female')), null=False, blank=False)
    ageatdiagnosis = models.DecimalField(db_column='AgeAtDiagnosis', max_digits=3, decimal_places=1, blank=True)
    smokingstatus = models.CharField(db_column='SmokingStatus', max_length=8, choices=(('Current', 'current'), ('Former', 'former'), ('Never', 'never')), blank=True, null=True)

    height = models.IntegerField(db_column='Height', blank=True, null=True)  # Field name made lowercase.
    weight = models.IntegerField(db_column='Weight', blank=True, null=True)  # Field name made lowercase.

    status = models.CharField(db_column='Status', max_length=20, choices=(('P', 'progressor'), ('NP', 'non-progressor')), null=False, blank=False)

    dateinitialdiagnosis = DateText(db_column='DateInitialDiagnosis', blank=True, null=True)
    dateprogressed = DateText(db_column='DateProgressed', blank=True, null=True)

    circumference = models.IntegerField(db_column='Circumference', null=True, blank=True, validators=[MaxValueValidator(50), MinValueValidator(0)])
    maximal = models.IntegerField(db_column='Maximal', null=True, blank=True, validators=[MaxValueValidator(50), MinValueValidator(0)])

    type = models.CharField(db_column='Type', max_length=10, default='Patient', null=False, choices=(('Patient', 'patient'), ('Organoid', 'organoid'), ('Cell line', 'cell line')))

    def __str__(self):
        return self.type + " ID:" + str(self.id) + " Sex:" + self.gender + " Age:" + str(self.ageatdiagnosis) +  " C" + str(self.circumference) + "M" + str(self.maximal)

    def _get_bmi(self):
        if (not self.height or not self.weight) or (self.height < 0 or self.weight < 0):
            return None
        else:
            return round(self.weight/((self.height/100)**2), 2)

    bmi = property(_get_bmi)

    class Meta:
        managed = False
        db_table = 'Patient'