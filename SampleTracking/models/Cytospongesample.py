from django.db import models

class Cytospongesample(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    Endoscopy = models.ForeignKey('Endoscopy', on_delete=models.CASCADE, db_column='EndoscopyID')
    #endoscopyid = models.IntegerField(db_column='EndoscopyID')  # Field name made lowercase.
    Patient = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID')
    #patientid = models.IntegerField(db_column='PatientID')  # Field name made lowercase.
    sampletype = models.TextField(db_column='SampleType')  # Field name made lowercase.
    pathology = models.CharField(db_column='Pathology', blank=True, null=True, max_length=20, choices=(('normal', 'normal'), ('GM', 'GM'), ('NDBE', 'NDBE'), ('ID', 'ID'), ('LGD', 'LGD'), ('HGD', 'HGD'), ('IMC', 'IMC'), ('EAC', 'EAC'), ('Other', 'Other')))
    pathnotes = models.BinaryField(db_column='PathNotes', blank=True, null=True)  # Field name made lowercase.
    p53ihc = models.TextField(db_column='p53IHC', blank=True, null=True)  # Field name made lowercase.
    site = models.TextField(db_column='Site')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CytospongeSample'
