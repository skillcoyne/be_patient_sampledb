from django.db import models
from SampleTracking.fields import DateText

# class HistoPathSlide(models.Model):
#     ID = models.AutoField(primary_key=True)
#     Endoscopy = models.ForeignKey('Endoscopy', on_delete=models.CASCADE, db_column='EndoscopyID')
#     EndoscopySample = models.ForeignKey('EndoscopySample', on_delete=models.CASCADE, db_column='EndoscopySampleID')
#     Patient = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID')
#     DigitalFileName = models.TextField(null=False)
#     ScannedBy = models.CharField(max_length=100, default='Pathgnomics')
#     DateScanned = DateText(blank=True, null=True)
#     Notes = models.TextField(blank=True)
#
#     def __str__(self):
#         return str(self.ID) + " " + self.DigitalFileName
#
#     class Meta:
#         db_table = "HistoPathSlides"

class HistoPathSlides(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    Endoscopy = models.ForeignKey('Endoscopy', on_delete=models.CASCADE, db_column='EndoscopyID')
    EndoscopySample = models.ForeignKey('EndoscopySample', on_delete=models.CASCADE, db_column='EndoscopySampleID')
    Patient = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID')

    #endoscopyid = models.IntegerField(db_column='EndoscopyID')  # Field name made lowercase.
    #endoscopysampleid = models.IntegerField(db_column='EndoscopySampleID')  # Field name made lowercase.
    #patientid = models.IntegerField(db_column='PatientID')  # Field name made lowercase.
    digitalfilename = models.TextField(db_column='DigitalFileName', blank=True, null=False)  # Field name made lowercase.
    scannedby = models.TextField(db_column='ScannedBy', blank=True, null=True)  # Field name made lowercase.
    datescanned = DateText(db_column='DateScanned', blank=True, null=True)
    notes = models.TextField(db_column='Notes', blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return str(self.id) + " " + self.digitalfilename

    class Meta:
        managed = False
        db_table = 'HistoPathSlides'

