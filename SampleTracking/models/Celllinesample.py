from django.db import models


class Celllinesample(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    cellline = models.TextField(db_column='CellLine')  # Field name made lowercase.
    sampletype = models.TextField(db_column='SampleType')  # Field name made lowercase.
    notes = models.BinaryField(db_column='Notes', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CellLineSample'

