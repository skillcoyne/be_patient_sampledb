from django.db import models

class Qualitycontrolsamples(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    sampleid = models.IntegerField(db_column='SampleID', null=False)  # Field name made lowercase.

    sampletype = models.TextField(db_column='SampleType')  # Field name made lowercase.
    pathology = models.CharField(db_column='Pathology', blank=True, null=True, max_length=20, choices=(('normal', 'normal'), ('GM', 'GM'), ('NDBE', 'NDBE'), ('ID', 'ID'), ('LGD', 'LGD'), ('HGD', 'HGD'), ('IMC', 'IMC'), ('EAC', 'EAC'), ('Other', 'Other')))

    tissuetype = models.CharField(db_column='TissueType', blank=True, null=True,  max_length=20, choices=(('cell line', 'cell line'), ('organoid', 'organoid'), ('tumour', 'tumour'), ('cytosponge', 'cytosponge')))  # Field name made lowercase.
    slx = models.CharField(db_column='SLX', max_length=100, null=False)
    plateindex = models.CharField(db_column='PlateIndex', max_length=100, null=False)  # Field name made lowercase.

    protocolnotes = models.BinaryField(db_column='ProtocolNotes', blank=True, null=True)  # Field name made lowercase.
    ra = models.TextField(db_column='RA', blank=True, null=True)  # Field name made lowercase.
    progressionstatus = models.CharField(db_column='ProgressionStatus', max_length=20, choices=(('P', 'progressor'), ('NP', 'non-progressor')), null=False, blank=False)
    notes = models.BinaryField(db_column='Notes', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'QualityControlSamples'

