from django.db import models
from SampleTracking.fields import DateText

#class Endoscopy(models.Model):
#    ID = models.AutoField(primary_key=True)
#    Patient = models.ForeignKey(to='Patient', on_delete=models.CASCADE, db_column='PatientID')
    #EndoscopyDate = models.DateTimeField(null=False, auto_now=False, auto_now_add=False)
#    EndoscopyDate = DateText(blank=True, null=False)
    # Needs to be case-insensitive
#    PathCaseID = models.CharField(max_length=100, null=False, unique=True, blank=False)
#    Notes = models.TextField(blank=True, null=True)

#    def __str__(self):
#        return "Endoscopy ID:" + str(self.ID) + " Date:" + self.EndoscopyDate.strftime("%Y-%m-%d") + ' Path case: ' + str(self.PathCaseID)

#    class Meta:
#        db_table = "Endoscopy"


class Endoscopy(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    Patient = models.ForeignKey(to='Patient', on_delete=models.CASCADE, db_column='PatientID')
    #patientid = models.IntegerField(db_column='PatientID')  # Field name made lowercase.
    endoscopydate = DateText(blank=True, null=False)
    #endoscopydate = models.TextField(db_column='EndoscopyDate')  # Field name made lowercase.
    pathcaseid = models.TextField(db_column='PathCaseID', unique=True, null=False, blank=False)  # Field name made lowercase.
    notes = models.BinaryField(db_column='Notes', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Endoscopy'

    def __str__(self):
        return "Endoscopy ID:" + str(self.id) + " Date:" + self.endoscopydate.strftime("%Y-%m-%d") + ' Path case: ' + str(self.pathcaseid)

