from django.db import models

#class AlternatePatientId(models.Model):
    #PatientID = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID')
#   AlternateID = models.CharField(null=False, max_length=100, db_column="AlternateID")
#   AltStudy = models.CharField(blank=True, max_length=100, db_column="AltIDStudy")
#   Patient = models.ForeignKey(to='Patient', on_delete=models.CASCADE, db_column='PatientID')

#    class Meta:
#        db_table = "AlternatePatientId"


class AlternatePatientId(models.Model):
    id = models.AutoField(db_column='id', unique=True, primary_key=True)  # Field name made lowercase.
    Patient = models.ForeignKey(to='Patient', on_delete=models.CASCADE, db_column='PatientID', null=False)
    #patientid = models.IntegerField(db_column='PatientID')  # Field name made lowercase.
    alternateid = models.TextField(db_column='AlternateID')  # Field name made lowercase.
    altidstudy = models.TextField(db_column='AltIDStudy', blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        return "Alt ID:" + str(self.alternateid) + " Patient ID:" + str(self.Patient.id)


    class Meta:
        managed = False
        db_table = 'AlternatePatientID'

