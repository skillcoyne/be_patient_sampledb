from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


# class EndoscopySample(models.Model):
#     ID = models.AutoField(primary_key=True)
#     Endoscopy = models.ForeignKey('Endoscopy', on_delete=models.CASCADE, db_column='EndoscopyID')
#     Patient = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID')
#     Block = models.CharField(max_length=100, null=False)
#     BlockLevel = models.IntegerField(null=True, validators=[MaxValueValidator(50), MinValueValidator(0)])
#     p53IHC = models.CharField(null=True, max_length=10, choices=(('normal', 'normal'), ('aberrant', 'aberrant')))
#     Pathology = models.CharField(null=False, max_length=20, choices=(('normal', 'normal'), ('GM', 'GM'), ('NDBE', 'NDBE'), ('ID', 'ID'), ('LGD', 'LGD'), ('HGD', 'HGD'), ('IMC', 'IMC'), ('EAC', 'EAC'), ('Other', 'Other')))
#     PathNotes = models.TextField(blank=True)
#     SampleType = models.CharField(max_length=100, default='FFPE', null=False)
#     Site = models.CharField(max_length=100, choices=(('Cambridge', 'Cambridge'), ('Belfast', 'Belfast'), ('Norfolk', 'Norfolk')))
#     Notes = models.TextField(blank=True)
#
#     def __str__(self):
#         out = "ID:" + str(self.ID) + " " + self.SampleType + " Pathology:" + self.Pathology + " Block: " + self.Block
#         if self.BlockLevel:
#             out = out + " " + str(self.BlockLevel)
#         return out
#
#
#     class Meta:
#         db_table = "EndoscopySample"

class EndoscopySample(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    Endoscopy = models.ForeignKey('Endoscopy', on_delete=models.CASCADE, db_column='EndoscopyID')
    #endoscopyid = models.IntegerField(db_column='EndoscopyID')  # Field name made lowercase.
    Patient = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID')
    #patientid = models.IntegerField(db_column='PatientID')  # Field name made lowercase.
    block = models.TextField(db_column='Block', null=False)  # Field name made lowercase.
    blocklevel = models.IntegerField(db_column='BlockLevel', blank=True, null=True, validators=[MaxValueValidator(50), MinValueValidator(0)])
    p53ihc = models.CharField(db_column='p53IHC', blank=True, null=True, max_length=10, choices=(('normal', 'normal'), ('aberrant', 'aberrant')))
    pathology = models.CharField(db_column='Pathology', blank=True, null=True, max_length=20, choices=(('normal', 'normal'), ('GM', 'GM'), ('NDBE', 'NDBE'), ('ID', 'ID'), ('LGD', 'LGD'), ('HGD', 'HGD'), ('IMC', 'IMC'), ('EAC', 'EAC'), ('Other', 'Other')))
    pathnotes = models.BinaryField(db_column='PathNotes', blank=True, null=True)  # Field name made lowercase.
    sampletype = models.TextField(db_column='SampleType')  # Field name made lowercase.
    site = models.TextField(db_column='Site')  # Field name made lowercase.
    sampletype = models.CharField(db_column='SampleType', max_length=100, default='FFPE', null=False)
    site = models.CharField(db_column='Site', max_length=100, choices=(('Cambridge', 'Cambridge'), ('Belfast', 'Belfast'), ('Norfolk', 'Norfolk')))
    notes = models.BinaryField(db_column='Notes', blank=True, null=True)  # Field name made lowercase.

    def __str__(self):
        out = "ID:" + str(self.id) + " " + self.sampletype + " Pathology:" + self.pathology + " Block: " + self.block
        if self.blocklevel:
            out = out + " " + str(self.blocklevel)
        return out

    class Meta:
        managed = False
        db_table = 'EndoscopySample'