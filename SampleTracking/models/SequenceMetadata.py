from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

class SequenceMetadata(models.Model):
    id = models.AutoField(db_column='ID', unique=True, primary_key=True)  # Field name made lowercase.
    Patient = models.ForeignKey('Patient', on_delete=models.CASCADE, db_column='PatientID', null=False)
    EndoscopySample = models.ForeignKey('EndoscopySample', on_delete=models.CASCADE, db_column='SampleID', null=False)  # Field name made lowercase.

    sampletable = models.CharField(db_column='SampleTable', max_length=20, null=False, choices=(('Endscopy', 'EndoscopySample'), ('Cytosponge', 'CytospongeSample'), ('Cell line', 'CellLineSample')), default='EndoscopySample')

    slx = models.CharField(db_column='SLX', max_length=100, null=False)
    plateindex = models.CharField(db_column='PlateIndex', max_length=100, null=False)  # Field name made lowercase.
    samplename = models.CharField(db_column='Samplename', max_length=150, null=False)  # Field name made lowercase.
    datesequenced = models.DateField(db_column='DateSequenced', blank=True, null=True, auto_now=False, auto_now_add=False)
    replicate = models.BooleanField(db_column='Replicate', default=False)  # Field name made lowercase.
    totalreads = models.IntegerField(db_column='TotalReads', blank=True, null=True, validators=[MinValueValidator(1)])

    readsmappedperc = models.IntegerField(db_column='ReadsMappedPerc', blank=True, null=True, validators=[MinValueValidator(0), MaxValueValidator(100)])
    duplicatereadsperc = models.IntegerField(db_column='DuplicateReadsPerc', blank=True, null=True, validators=[MinValueValidator(0), MaxValueValidator(100)])
    meanreadlength = models.IntegerField(db_column='MeanReadLength', blank=True, null=True, validators=[MinValueValidator(1), MaxValueValidator(300)])

    singleend = models.BooleanField(db_column='SingleEnd', default=True)
    meancoverage = models.DecimalField(db_column='MeanCoverage', blank=True, null=True, max_digits=2, decimal_places=2, validators=[MinValueValidator(0.1), MaxValueValidator(100)])
    bamfilename = models.BinaryField(db_column='BAMFilename', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SequenceMetadata'

    def __str__(self):
        return "ID:" + str(self.id) + " SLX:" + self.slx + " Plate index:" + self.plateindex

