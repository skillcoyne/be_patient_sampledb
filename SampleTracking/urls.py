
from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('patient/<int:patient_id>', views.detail, name='detail'),
    path('patient/search_results', views.search_results, name='search_results')
]