from django.shortcuts import render

from SampleTracking.models import Patient, Endoscopy, HistoPathSlides, SequenceMetadata


# Create your views here.
def index(request):
    context = {'patients': Patient.objects.count(), 'endoscopies': Endoscopy.objects.count(), 'slides': HistoPathSlides.objects.count(), 'sequences': SequenceMetadata.objects.count() }
    return render(request, 'patient/index.html', context)


def detail(request, patient_id):
    patient = Patient.objects.get(id=patient_id)

    print( patient.endoscopy_set.first().endoscopysample_set.first().histopathslides_set.first() )

    context = {'patient': patient}
    return render(request, 'patient/detail.html', context)


def search_results(request):
    if request.method == 'GET':
        patientID = request.GET.get('patient_id', None)
        status = request.GET.get('status', None)
        gender = request.GET.get('gender', None)
        smoking = request.GET.get('smoking', None)
        minAge = request.GET.get('minAge', None)
        maxAge = request.GET.get('maxAge', None)
        pragueM = request.GET.get('pragueM', None)
        pragueC = request.GET.get('pragueC', None)
        slide = request.GET.getlist('slide', None)
        seq = request.GET.getlist('seq', None)

        print(request.GET)

        hasslide = False;
        hasseq = False
        if len(slide) > 0:
            hasslide = True
        if len(seq) > 0:
            hasseq = True

        qset = Patient.objects.all()

        # ap = qset.order_by('ageatdiagnosis').first()
        #        if float(minAge) <= ap.ageatdiagnosis:
        #            minAge = None

        if patientID:
            qset = qset.filter(alternatepatientid__alternateid__contains=patientID)
        if minAge:
            qset = qset.filter(ageatdiagnosis__gte=minAge)
        if maxAge:
            qset = qset.filter(ageatdiagnosis__lte=maxAge)
        if gender:
            qset = qset.filter(gender=gender)
        if status:
            qset = qset.filter(status=status)
        if smoking:
            qset = qset.filter(smokingstatus=smoking)
        if pragueM:
            qset = qset.filter(maximal__lte=pragueM)
        if pragueC:
            qset = qset.filter(circumference__lte=pragueC)
        if hasslide:
            qset = qset.filter(histopathslides__isnull=False)
        if hasseq:
            qset = qset.filter(sequencemetadata__isnull=False)

        prague = None
        if pragueC or pragueM:
            prague = 'C' + str(pragueC) + 'M' + str(pragueM)

        age = None
        if minAge or maxAge:
            age = str(minAge) + '-' + str(maxAge)

        query = {'altid': patientID, 'status': status, 'gender': gender, 'smoking': smoking, 'prague': prague, 'age': age, 'slide': hasslide, 'seq': hasseq}

        context = {'results': qset, 'query': query}

    return render(request, 'patient/search_results.html', context)
