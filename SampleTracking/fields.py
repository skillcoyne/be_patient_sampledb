from django.db import models
from dateutil import parser

class DateText(models.Field):

    description = "Date field from text"

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 104
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return parser.parse(value).date()

    def get_prep_value(self, value):
        if value is None:
            return value
        return str(parser.parse(value).date())
